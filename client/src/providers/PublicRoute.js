import { Route, Redirect } from 'react-router-dom';
import React from 'react';


const PublicRoute = ({ component: Component, isLoggedIn, ...rest }) => {

  return (
    <Route {...rest} render={(props) => isLoggedIn ? <Redirect exact to='/home' /> : <Component {...props} />} />
  );
};

export default PublicRoute;