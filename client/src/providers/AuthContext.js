import { createContext } from 'react';
import decode from 'jwt-decode';

export const AuthContext = createContext({
  isLoggedIn: false,
  user: null,
  setLogInState: () => { }
})

export const getToken = () => {
  return (localStorage.getItem('token') || '');
};

export const extractUser = token => {
  try {
    return decode(token);
  } catch (error) {
    return null;
  }
};
