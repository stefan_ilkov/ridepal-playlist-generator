import React from 'react';
import LinearProgress from '@material-ui/core/LinearProgress';
import './Loading.css';

const Loading = () => {
  return <div className="Loading"><LinearProgress /></div>;
};

export default Loading;
