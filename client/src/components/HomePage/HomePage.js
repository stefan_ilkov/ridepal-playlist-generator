import TopAppBar from '../TopAppBar/TopAppBar';
import Grid from '@material-ui/core/Grid';
import { Typography } from '@material-ui/core';
import { makeStyles } from '@material-ui/core/styles';
import PlaylistList from '../PlaylistList/PlaylistList';
import { useEffect, useState } from 'react';
import { CONSTANTS } from '../../common/constants';
import UserMessage from '../UserMessage/UserMessage';


const useStyles = makeStyles({
  gridContainer: {
    padding: '15px',
    margin: 0,
    width: '100%'
  },
  root: {
    height: '100vh'
  }
});

const HomePage = () => {

  const userMsgClose = () => {
    setUserMessage({ open: false });
  };

  const [topThreePlaylists, setTopThreePlaylists] = useState([]);
  const [loading, setLoading] = useState(true);
  const [userMessage, setUserMessage] = useState({
    open: false,
    message: '',
    severity: ''
  });

  useEffect(() => {
    fetch(`${CONSTANTS.API_URL}/playlists/topthree`)
      .then(res => res.json())
      .then(data => {
        if (Array.isArray(data)) {
          setTopThreePlaylists(data);
        } else {
          throw new Error(data.error);
        }
      })
      .catch(error => setUserMessage({
        open: true,
        severity: 'error',
        message: `${error}`
      }))
      .finally(() => setLoading(false));
  }, []);

  const classes = useStyles();
  return (
    <div className="App">
      <TopAppBar />
      <Grid item xs={12} xl={12} container spacing={3} className={classes.gridContainer}>
        <Grid item xs={12}>
          <Typography variant="h5" align="center">
            3 Most Popular Playists
          </Typography>
        </Grid>
        <PlaylistList loading={loading} playlists={topThreePlaylists} />
      </Grid>
      <UserMessage
        open={userMessage.open}
        handleClose={userMsgClose}
        severity={userMessage.severity}
        message={<span>{userMessage.message}</span>} />
    </div>
  );
}

export default HomePage;
