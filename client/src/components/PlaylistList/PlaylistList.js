import { Grid, Typography } from '@material-ui/core';
import React from 'react';
import Loading from '../../Pages/Loading/Loading';
import SinglePlaylist from '../SinglePlaylist/SinglePlaylist';

const PlaylistList = ({ loading, playlists }) => {

  return (
    <>
      {loading
        ? <Grid item xs={12}>< Loading /></Grid >
        : playlists.length === 0
          ? <Grid item xs={12}>
            <Typography variant="h5" align="center">No playlists to show...</Typography>
          </Grid >
          : <>
            {playlists.map(playlist =>
              <Grid item xs={12} sm={6} md={4} lg={3} key={playlist.id}>
                <SinglePlaylist />
              </Grid>
            )}
          </>
      }
    </>
  );
};

export default PlaylistList;
