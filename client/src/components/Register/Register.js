import React, { useState, useContext } from 'react';
import Avatar from '@material-ui/core/Avatar';
import Button from '@material-ui/core/Button';
import CssBaseline from '@material-ui/core/CssBaseline';
import TextField from '@material-ui/core/TextField';
import Link from '@material-ui/core/Link';
import Grid from '@material-ui/core/Grid';
import LockOutlinedIcon from '@material-ui/icons/LockOutlined';
import Typography from '@material-ui/core/Typography';
import { makeStyles } from '@material-ui/core/styles';
import Paper from '@material-ui/core/Paper';
import { CONSTANTS } from '../../common/constants';
import UserMessage from '../UserMessage/UserMessage';
import { AuthContext } from '../../providers/AuthContext';


const useStyles = makeStyles((theme) => ({
  root: {
    height: '100vh',
  },
  paper: {
    margin: theme.spacing(8, 4),
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'center',
  },
  image: {
    backgroundImage: 'url(https://source.unsplash.com//weekly/?music)',
    backgroundRepeat: 'no-repeat',
    backgroundColor:
      theme.palette.type === 'light' ? theme.palette.grey[50] : theme.palette.grey[900],
    backgroundSize: 'cover',
    backgroundPosition: 'center',
  },
  avatar: {
    margin: theme.spacing(1),
    backgroundColor: theme.palette.secondary.main,
  },
  form: {
    width: '100%', // Fix IE 11 issue.
    marginTop: theme.spacing(2),
  },
  submit: {
    margin: theme.spacing(3, 0, 2),
  },
}));

const Register = (props) => {
  const classes = useStyles();

  const auth = useContext(AuthContext);
  const [userMessage, setUserMessage] = useState({
    open: false,
    severity: '',
    message: '',
  });

  const [isFormValid, setIsFormValid] = useState(false);

  const [form, setForm] = useState({
    firstName: {
      placeholder: 'First Name',
      value: '',
      validations: {
        required: true,
        minLength: 2,
        maxLength: 50
      },
      valid: true,
      touched: false
    },
    lastName: {
      placeholder: 'Last Name',
      value: '',
      validations: {
        required: true,
        minLength: 2,
        maxLength: 50
      },
      valid: true,
      touched: false
    },
    username: {
      placeholder: 'Username',
      value: '',
      validations: {
        required: true,
        minLength: 3,
        maxLength: 15
      },
      valid: true,
      touched: false
    },
    email: {
      placeholder: 'Email Address',
      value: '',
      validations: {
        required: true,
        minLength: 3,
        maxLength: 50,
        pattern: (/^[^\s@,]+@[^\s@,]+\.[^\s@,]+$/)
      },
      valid: true,
      touched: false
    },
    password: {
      placeholder: 'Password',
      value: '',
      validations: {
        required: true,
        minLength: 3,
        maxLength: 20,
      },
      valid: true,
      touched: false
    },
    confirmPassword: {
      placeholder: 'Confirm Password',
      value: '',
      validations: {
        required: true,
        minLength: 3,
        maxLength: 20,
        confirm: true,
      },
      valid: true,
      touched: false
    }
  })

  const isInputValid = (input, validations) => {
    let isValid = true;

    if (validations.required) {
      isValid = isValid && input.length !== 0;
    }

    if (validations.minLength) {
      isValid = isValid && input.length >= validations.minLength;
    }

    if (validations.maxLength) {
      isValid = isValid && input.length <= validations.maxLength;
    }

    if (validations.pattern) {
      isValid = isValid && (validations.pattern).test(input);
    }

    if (validations.confirm) {
      isValid = isValid && form['password'].value === input;
    }

    return isValid;
  }

  const handleInputChange = (event) => {
    const { name, value } = event.target;

    const updatedElement = { ...form[name] };
    updatedElement.value = value;
    updatedElement.touched = true;

    updatedElement.valid = isInputValid(value, updatedElement.validations)
    const updatedForm = { ...form, [name]: updatedElement };
    setForm(updatedForm);

    if (name === 'password') {
      form['confirmPassword'].valid = updatedElement.value === form['confirmPassword'].value;
    }
    const formValid = Object.values(updatedForm).every(elem => elem.valid && elem.touched);
    setIsFormValid(formValid);
  }

  const register = () => {
    const body = {};
    Object.entries(form).map(elem => {
      if (elem[0] === 'firstName') {
        body['first_name'] = elem[1].value;
      } else if (elem[0] === 'lastName') {
        body['last_name'] = elem[1].value;
      } else {
        body[elem[0]] = elem[1].value;
      }
      return '';
    });
    fetch(`${CONSTANTS.API_URL}/auth/register`, {
      method: 'POST',
      headers: { 'Content-Type': 'application/json', },
      body: JSON.stringify(body),
    })
      .then(data => data.json())
      .then(res => {
        if (res.error) {
          throw new Error(res.error);
        } else {
          setUserMessage({
            open: true,
            severity: 'success',
            message: 'You have been registered! Redirecting...',
          })
          auth.setLogInState({ user: { username: res.username, password: form.password.value } });
          setTimeout(() => props.history.push('/login'), 2000)
        }
      })
      .catch(err => setUserMessage({
        open: true,
        severity: 'error',
        message: `${err}`,
      }))
  }

  const userMsgClose = () => {
    setUserMessage({ open: false });
  }

  const handleEnterKeyPress = (e) => {
    if (e.key === 'Enter') {
      register();
    }
  }

  const formElements = Object.keys(form)
    .map(name => {
      return {
        id: name,
        config: form[name]
      }
    })
    .map(({ id, config }) => {
      return (
        <Grid key={id} item xs={12} sm={id === 'firstName' || id === 'lastName' ? 6 : 12}>
          <TextField
            name={id}
            variant="outlined"
            required={!config.required}
            id={id}
            fullWidth
            label={config.placeholder}
            autoFocus
            onKeyPress={handleEnterKeyPress}
            onChange={(e) => handleInputChange(e)}
            type={id === 'password' || id === 'confirmPassword' ? 'password' : ''}
            color='primary'
            helperText={!config.valid && config.value.length > 0 ? id === 'confirmPassword' ?
              'Password does not match' : 'Incorrect field input' : ''}
            error={config.touched && !config.valid && config.value.length > 0 ? true : false}
          />
        </Grid>
      )
    })

  return (
    <Grid container component="main" className={classes.root}>
      <CssBaseline />
      <Grid item xs={false} sm={4} md={7} className={classes.image} />
      <Grid item xs={12} sm={8} md={5} component={Paper} elevation={6} square>
        <div className={classes.paper}>
          <Avatar className={classes.avatar}>
            <LockOutlinedIcon />
          </Avatar>
          <Typography component="h1" variant="h5">
            Sign up
          </Typography>
          <form className={classes.form} noValidate>
            <Grid container spacing={2}>
              {formElements}
            </Grid>
            <Button
              // type="submit"
              fullWidth
              variant="contained"
              color="primary"
              className={classes.submit}
              onClick={register}
              disabled={!isFormValid}
            >
              Sign Up
            </Button>
            <Grid container >
              <Grid item>
                <Link href="/login" variant="body2">
                  Already have an account? Sign in
                </Link>
              </Grid>
            </Grid>
          </form>
        </div>
        <UserMessage
          open={userMessage.open}
          handleClose={userMsgClose}
          severity={userMessage.severity}
          message={userMessage.message} />
      </Grid>
    </Grid>
  );
}

export default Register;