import React, { useContext, useState } from 'react';
import Avatar from '@material-ui/core/Avatar';
import Button from '@material-ui/core/Button';
import CssBaseline from '@material-ui/core/CssBaseline';
import TextField from '@material-ui/core/TextField';
import Link from '@material-ui/core/Link';
import Paper from '@material-ui/core/Paper';
import Box from '@material-ui/core/Box';
import Grid from '@material-ui/core/Grid';
import LockOutlinedIcon from '@material-ui/icons/LockOutlined';
import Typography from '@material-ui/core/Typography';
import { makeStyles } from '@material-ui/core/styles';
import { AuthContext } from '../../providers/AuthContext';
import decode from 'jwt-decode';
import { CONSTANTS } from '../../common/constants';
import UserMessage from '../UserMessage/UserMessage';

function Copyright() {
  return (
    <Typography variant="body2" color="textSecondary" align="center">
      {'Copyright © '}
      <Link color="inherit" href="https://material-ui.com/">
        Your Website
      </Link>{' '}
      {new Date().getFullYear()}
      {'.'}
    </Typography>
  );
}

const useStyles = makeStyles((theme) => ({
  root: {
    height: '100vh',
  },
  image: {
    backgroundImage: 'url(https://source.unsplash.com//daily/?music)',
    backgroundRepeat: 'no-repeat',
    backgroundColor:
      theme.palette.type === 'light' ? theme.palette.grey[50] : theme.palette.grey[900],
    backgroundSize: 'cover',
    backgroundPosition: 'center',
  },
  paper: {
    margin: theme.spacing(8, 4),
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'center',
  },
  avatar: {
    margin: theme.spacing(1),
    backgroundColor: theme.palette.secondary.main,
  },
  form: {
    width: '100%', // Fix IE 11 issue.
    marginTop: theme.spacing(1),
  },
  submit: {
    margin: theme.spacing(3, 0, 2),
  },
}));

const Login = (props) => {
  const classes = useStyles();
  const auth = useContext(AuthContext);

  const [isFormValid, setIsFormValid] = useState(auth.user ? true : false);

  const [form, setForm] = useState({
    username: {
      placeholder: 'Enter username',
      value: auth.user ? auth.user.username : '',
      validations: {
        required: true,
      },
      valid: true,
      touched: auth.user ? true : false
    },
    password: {
      placeholder: 'Enter password',
      value: auth.user ? auth.user.password : '',
      validations: {
        required: true,
      },
      valid: true,
      touched: auth.user ? true : false
    }
  })

  const isInputValid = (input, validations) => {
    let isValid = true;

    if (validations.required) {
      isValid = isValid && input.length !== 0;
    }

    return isValid;
  }


  const handleInputChange = (event) => {
    const { name, value } = event.target;

    const updatedElement = { ...form[name] };
    updatedElement.value = value;
    updatedElement.touched = true;
    updatedElement.valid = isInputValid(value, updatedElement.validations);

    const updatedForm = { ...form, [name]: updatedElement };
    setForm(updatedForm);

    const formValid = Object.values(updatedForm).every(elem => elem.valid && elem.touched);
    setIsFormValid(formValid)
  }

  const [userMessage, setUserMessage] = useState({
    open: false,
    message: '',
    severity: ''
  });

  const login = () => {
    const body = {};

    Object.entries(form).map(element => {
      body[element[0]] = element[1].value;
      return '';
    })
    fetch(`${CONSTANTS.API_URL}/auth/login`, {
      method: 'POST',
      headers: { 'Content-Type': 'application/json', },
      body: JSON.stringify(body),
    })
      .then(data => data.json())
      .then(data => {
        if (data.error) {
          throw new Error(data.error)
        } else {
          const user = decode(data.token);
          localStorage.setItem('token', data.token);
          setUserMessage({
            open: true,
            severity: 'success',
            message: `You have been logged in successfully! Redirecting...`
          })
          setTimeout(() => auth.setLogInState({ user, isLoggedIn: true }), 2000);
          ;
        }
      })
      .catch(err => {
        setUserMessage({
          open: true,
          severity: 'error',
          message: `${err}`
        })
      });
  };

  const userMsgClose = () => {
    setUserMessage({ open: false });
  };

  const handleEnterKeyPress = (e) => {
    if (e.key === 'Enter') {
      login();
    }
  };

  const formElements = Object.keys(form)
    .map(name => {
      return {
        id: name,
        config: form[name]
      }
    })
    .map(({ id, config }) => {
      return (
        <TextField
          key={id}
          variant="outlined"
          margin="normal"
          required
          fullWidth
          id={id}
          label={config.placeholder}
          name={id}
          type={id === 'password' ? 'password' : ''}
          autoComplete={id}
          autoFocus
          defaultValue={config.value}
          onKeyPress={handleEnterKeyPress}
          onChange={(e) => handleInputChange(e)}
          helperText={!config.valid ? 'Incorrect input' : ''}
          error={config.touched && !config.valid ? true : false}
        />
      )
    })

  return (
    <Grid container component="main" className={classes.root}>
      <CssBaseline />
      <Grid item xs={false} sm={4} md={7} className={classes.image} />
      <Grid item xs={12} sm={8} md={5} component={Paper} elevation={6} square>
        <div className={classes.paper}>
          <Avatar className={classes.avatar}>
            <LockOutlinedIcon />
          </Avatar>
          <Typography component="h1" variant="h5">
            Sign in
          </Typography>
          <form className={classes.form} noValidate>
            {/* <TextField
              variant="outlined"
              margin="normal"
              required
              fullWidth
              id="email"
              label="Username or Email Address"
              name="username"
              autoComplete="username"
              autoFocus
              defaultValue={user.username}
              onKeyPress={handleEnterKeyPress}
              onChange={(e) => updateUser('username', e.target.value)}
            />
            <TextField
              variant="outlined"
              margin="normal"
              required
              fullWidth
              name="password"
              label="Password"
              type="password"
              id="password"
              autoComplete="current-password"
              onKeyPress={handleEnterKeyPress}
              defaultValue={user.password}
              onChange={(e) => updateUser('password', e.target.value)}
            /> */}
            {formElements}
            <Button
              //type="submit"
              fullWidth
              variant="contained"
              color="primary"
              className={classes.submit}
              onClick={login}
              disabled={!isFormValid}
            >
              Sign In
            </Button>
            <Grid container>
              <Grid item>
                <Link href="/register" variant="body2">
                  {"Don't have an account? Sign Up"}
                </Link>
              </Grid>
            </Grid>
            <Box mt={5}>
              <Copyright />
            </Box>
          </form>
        </div>
        <UserMessage
          open={userMessage.open}
          handleClose={userMsgClose}
          severity={userMessage.severity}
          message={<span>{userMessage.message}</span>} />
      </Grid>
    </Grid>
  );
}

export default Login;