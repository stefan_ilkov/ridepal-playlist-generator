import './App.css';
import { BrowserRouter, Redirect, Route, Switch } from 'react-router-dom';
import Login from './components/Login/Login';
import Register from './components/Register/Register';
import HomePage from './components/HomePage/HomePage';
import { AuthContext, getToken, extractUser } from './providers/AuthContext';
import { useState } from 'react';
import PublicRoute from './providers/PublicRoute';
import NotFound from './Pages/NotFound/NotFound';
import GuardedRoute from './providers/GuardedRoute';
function App() {

  const token = getToken();
  const [authValue, setAuthValue] = useState({
    isLoggedIn: token ? true : false,
    user: extractUser(token)
  });

  return (
    <BrowserRouter>
      <AuthContext.Provider
        value={{ ...authValue, setLogInState: setAuthValue }}>
        <div className="App">
          <Switch>
            {/* Public Routes */}
            <Redirect path='/' exact to='/home' />
            <PublicRoute path='/register' isLoggedIn={authValue.isLoggedIn} exact component={Register} />
            <PublicRoute path='/login' isLoggedIn={authValue.isLoggedIn} exact component={Login} />
            <GuardedRoute path='/home' isLoggedIn={authValue.isLoggedIn} exact component={HomePage} />

            <Route path="*" component={NotFound} />
          </Switch>
        </div>
      </AuthContext.Provider>
    </BrowserRouter >
  );
}

export default App;
