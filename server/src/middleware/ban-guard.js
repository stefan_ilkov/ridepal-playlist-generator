import usersData from '../data/users-data.js';

export default async (req, res, next) => {
  const activeBans = await usersData.getActiveBan(req.user.user_id);

  if (activeBans) {
    return res.status(403).json({ error: 'Sorry, you\'re banned! ' });
  }

  await next();
};
