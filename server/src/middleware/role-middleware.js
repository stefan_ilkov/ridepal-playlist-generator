export const roleAuthorization = (role) => (req, res, next) => {

  if (req.user.role_title !== role) {
    return res.status(403).json({ error: 'Sorry, you\'re not authorized!' });
  }

  next();
};
