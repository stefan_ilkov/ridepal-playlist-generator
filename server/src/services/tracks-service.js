import serviceErrors from './service-errors.js';

const getAllTracks = tracksData => {
  return async (sort, page, limit) => {
    return await tracksData.getAll(sort, page, limit);
  };
};
export default {
  getAllTracks,
};