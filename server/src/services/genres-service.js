import serviceErrors from './service-errors.js';

const getAllGenres = genresData => {
  return async (sort, page, limit) => {
    return await genresData.getAll(sort, page, limit);
  };
};
export default {
  getAllGenres,
};