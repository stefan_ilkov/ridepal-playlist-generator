import serviceErrors from './service-errors.js';

const getAllArtists = artistsData => {
  return async () => {

    const allArtists = await artistsData.getAllArtists();

    return allArtists;
  };
};
export default {
  getAllArtists,
};