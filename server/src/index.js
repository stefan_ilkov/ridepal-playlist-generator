import cors from 'cors';
import helmet from 'helmet';
import express from 'express';
import adminController from './controllers/admin-controller.js';
import albumsController from './controllers/albums-controller.js';
import genresController from './controllers/genres-controller.js';
import tracksController from './controllers/tracks-controller.js';
import artistsController from './controllers/artists-controller.js';
import usersController from './controllers/users-controller.js';
import authController from './controllers/auth-controller.js';
import playlistsController from './controllers/playlists-controller.js';
import { PORT } from './config.js';
import jwtStrategy from './auth/strategy.js';
import passport from 'passport';

const app = express();

app.use(cors());
app.use(helmet());
app.use(express.json());

passport.use(jwtStrategy);
app.use(passport.initialize());


app.use('/admin', adminController);
app.use('/playlists', playlistsController);
app.use('/artists', artistsController);
app.use('/albums', albumsController);
app.use('/genres', genresController);
app.use('/tracks', tracksController);
app.use('/users', usersController);
app.use('/auth', authController);

// return message if endopoint does not exist
app.all('*', (req, res) =>
  res.status(404).send({ error: 'Resource not found!' }),
);

app.listen(PORT, () => console.log(`Listening on ${PORT}...`));
