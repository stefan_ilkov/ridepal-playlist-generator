import express from 'express';
import { authMiddleware } from '../middleware/auth.middleware.js';
import loggedUserMiddleware from '../middleware/logged-user-middleware.js';

const albumsController = express.Router();

albumsController.use(authMiddleware);

albumsController
// get all albums
  .get('/', loggedUserMiddleware, async (req, res) => {

    res.status(200);
  });

export default albumsController;
