import express from 'express';
import genresData from '../data/genres-data.js';
import genresService from '../services/genres-service.js';


const genresController = express.Router();

genresController
  // get all genres
  .get('/', async (req, res) => {
    const { search, sort, page, limit } = req.query;
    const genres = await genresService.getAllGenres(genresData)(search, sort, page, limit);

    res.status(200).json(genres);
  });

export default genresController;
