import express from 'express';
import { authMiddleware } from '../middleware/auth.middleware.js';
import artistsData from '../data/artists-data.js';
import artistsService from '../services/artists-service.js';
import loggedUserMiddleware from '../middleware/logged-user-middleware.js';

const artistsController = express.Router();

artistsController.use(authMiddleware);

artistsController
// get all artists
  .get('/', loggedUserMiddleware, async (req, res) => {

    const artists = await artistsService.getAllArtists(artistsData)();
    res.status(200).json(artists);
  });

export default artistsController;
