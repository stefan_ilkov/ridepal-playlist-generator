import express from 'express';
import { authMiddleware } from '../middleware/auth.middleware.js';
import usersData from '../data/users-data.js';
import usersService from '../services/users-service.js';
import serviceErrors from '../services/service-errors.js';
import { roleAuthorization } from '../middleware/role-middleware.js';
import userRoles from '../common/user-roles.js';
import loggedUserMiddleware from '../middleware/logged-user-middleware.js';

const usersController = express.Router();

usersController.use(authMiddleware);
usersController.use(loggedUserMiddleware);

usersController
  // get all users
  .get('/', roleAuthorization(userRoles.ADMIN), async (req, res) => {
    const users = await usersService.getAllUsers(usersData)();
    res.status(200).json(users);
  })
  // update user role
  .put('/', async (req, res) => {
    const users = await usersService.updateUserProfile(usersData)();
    res.status(200).json(users);
  })
  // restore user
  .put('/:id', roleAuthorization(userRoles.ADMIN), async (req, res) => {
    const { id } = req.params;

    const { error, user } = await usersService.restoreUser(usersData)(+id);

    if (error === serviceErrors.RECORD_NOT_FOUND) {
      return res.status(404).json({ msg: `User with id ${req.params.id} was not found!` });
    } else {
      res.status(200).json(user);
    }
  })
  // delete user
  .delete('/:id', roleAuthorization(userRoles.ADMIN), async (req, res) => {
    const { id } = req.params;

    if (req.user.user_id === +id) {
      return res.status(409).json({ error: 'You cannot delete yourself!' });
    }

    const { error, user } = await usersService.removeUser(usersData)(+id);

    if (error === serviceErrors.RECORD_NOT_FOUND) {
      return res.status(404).json({ msg: `User with id ${req.params.id} was not found!` });
    } else {
      res.status(200).json(user);
    }
  });
export default usersController;
