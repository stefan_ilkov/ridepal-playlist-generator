import { TITLE_MAX_LENGTH, TITLE_MIN_LENGTH } from '../common/constants.js';

export default {
  start_point: (value) => typeof value === 'string',
  end_point: (value) => typeof value === 'string',
  title: (value) => typeof value === 'string' && value.length > TITLE_MIN_LENGTH && value.length < TITLE_MAX_LENGTH,
  repeat_artists: (value) => typeof value === 'boolean',
  genres: (value) => Array.isArray(value),
};