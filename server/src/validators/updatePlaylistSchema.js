import { TITLE_MAX_LENGTH, TITLE_MIN_LENGTH } from '../common/constants.js';

export default {
  title: (value) => typeof value === 'undefined' || (typeof value === 'string' && value.length > TITLE_MIN_LENGTH && value.length < TITLE_MAX_LENGTH),
};  