export const createUserSchema = {
  username: value => typeof value === 'string' && value.length > 3,
  password: value => typeof value === 'string' && value.length >= 6,
  email: value => typeof value === 'string' && (/^[^\s@,]+@[^\s@,]+\.[^\s@,]+$/).test(value),
};