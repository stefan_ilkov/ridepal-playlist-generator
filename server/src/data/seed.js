/* eslint-disable quotes */
import db from './pool.js';
import fetch from 'node-fetch';
import bcrypt from 'bcrypt';

(async () => {

  const { data: genres } = await fetch('https://api.deezer.com/genre')
    .then(res => res.json())
    .catch(error => {
      console.log(error);
    });

  const genresFiltered = genres.filter(genre => {
    if (genre.id === 152 || genre.id === 116 || genre.id === 132 || genre.id === 129) {
      return genre;
    }
  });

  await Promise.all(genresFiltered.map(({ id, name, picture }) => {
    db.query(`INSERT INTO genres (d_id, name, d_pic)
  VALUES (?, ?, ?)`, [id, name, picture]);
  }));


  for (const genre of genresFiltered) {
    const { data: artists } = await fetch(`https://api.deezer.com/genre/${genre.id}/artists`)
      .then(res => res.json())
      .catch(error => {
        console.log(error);
      });

    await Promise.all(artists.map(async (artist) => {
      if (/^[a-zA-Z0-9_ ]*$/.test(artist.name)) {
        const checkIfArtistExists = await db.query(`SELECT name FROM artists WHERE d_id=${artist.id}`);

        if (!checkIfArtistExists[0]) {
          await db.query(`INSERT INTO artists (d_id, d_pic, name, tracklist_url)
          VALUES (${artist.id}, '${artist.picture}', '${artist.name}', '${artist.tracklist}')`);

          await db.query(`INSERT INTO artist_genres (artist_id, genre_id)
          VALUES ((SELECT id FROM artists WHERE d_id=${artist.id}), ${genre.id})`);
        }

        if (checkIfArtistExists[0]) {
          await db.query(`INSERT INTO artist_genres (artist_id, genre_id)
          VALUES ((SELECT id FROM artists WHERE d_id=${artist.id}), ${genre.id})`);
        }
      }
    }));
  }

  const artists = await db.query(`SELECT id, d_id FROM artists`);

  for (const artist of artists) {
    const { data: albums } = await fetch(`https://api.deezer.com/artist/${artist.d_id}/albums?limit=7`)
      .then(res => res.json())
      .catch(error => {
        console.log(error);
      });
    albums ?
      await Promise.all(albums.map(async (album) => {
        if (/^[a-zA-Z0-9_ ]*$/.test(album.title)) {
          const checkGenre = await db.query(`SELECT d_id FROM genres WHERE d_id=${album.genre_id}`);

          const checkIfAlbumExists = await db.query(`SELECT id FROM albums WHERE d_id=${album.id}`);
          if (checkGenre[0] && !checkIfAlbumExists[0] && album.tracklist) {
            await db.query(`INSERT INTO albums (d_id, title, d_cover, d_release_date, d_fans, album_tracklist_url, genre_id)
              VALUES (${album.id}, '${album.title}', '${album.cover}', 
                '${album.release_date}', ${album.fans}, '${album.tracklist}', ${album.genre_id})`);

            await db.query(`INSERT INTO artist_albums (artist_id, album_id)
          VALUES ((SELECT id FROM artists WHERE d_id=${artist.d_id}), (SELECT id FROM albums WHERE d_id=${album.id}))`);
          }

          if (checkGenre[0] && checkIfAlbumExists[0]) {
            await db.query(`INSERT INTO artist_albums (artist_id, album_id)
            VALUES ((SELECT id FROM artists WHERE d_id=${artist.d_id}), (SELECT id FROM albums WHERE d_id=${album.id}))`);
          }
        }
      })) :
      console.log('No album found');
  }

  const albums = await db.query(`SELECT id, genre_id, d_id, album_tracklist_url FROM albums`);

  for (const album of albums) {
    const { data: tracks } = await fetch(`${album.album_tracklist_url}`)
      .then(res => res.json())
      .catch(error => {
        console.log(error);
      });

    await Promise.all(tracks.map(async (track) => {
      if (/^[a-zA-Z0-9_ ]*$/.test(track.title)) {
        const checkArtist = await db.query(`SELECT name FROM artists WHERE d_id=${track.artist.id}`);

        if (checkArtist[0]) {
          await db.query(`INSERT INTO tracks (d_id, title, duration, link, rank, preview_url, genre_id, album_id, artist_id)
        VALUES (${track.id}, '${track.title}', ${parseInt(track.duration)}, 
          '${track.link}', ${parseInt(track.rank)}, '${track.preview}',
          ${album.genre_id}, (SELECT id FROM albums WHERE d_id=${album.d_id}), (SELECT id FROM artists WHERE name='${track.artist.name}'))`);
        }
      }
    }));
  }

  const roles = [
    { role: 'Admin' },
    { role: 'User' },
  ];

  await Promise.all(roles.map(({ role }) => db.query(`INSERT INTO roles (role_title)
  VALUES (?)`, [role])));

  const reactions = [
    { reaction: 'Like' },
    { reaction: 'Dislike' },
    { reaction: 'Love' },
  ];

  await Promise.all(reactions.map(({ reaction }) => db.query(`INSERT INTO reactions (reaction)
  VALUES (?)`, [reaction])));

  const result1 = await db.query('SELECT * FROM users WHERE username = ?', ['admin']);

  if (!result1 || result1.length === 0) {
    console.log('Creating admin...');

    await db.query(`
    INSERT INTO users (role_id, first_name, last_name, username, password, email)
    VALUES (?, ?, ?, ?, ?, ?)
  `, [1, 'Stefan', 'Ilkov', 'stefan', await bcrypt.hash('password', 10), 'stefan@test.com']);

    console.log(`Admin with username: 'stefan' and password: 'password' has been created`);

    await db.query(`
    INSERT INTO users (role_id, first_name, last_name, username, password, email)
    VALUES (?, ?, ?, ?, ?, ?)
  `, [1, 'Iva', 'Ilieva', 'iva', await bcrypt.hash('123456', 10), 'iva@test.com']);

    console.log(`Admin with username: 'iva' and password: '123456' has been created`);

  } else {
    console.log('Admin has already been created.');
  }

  db.end();
})()
  .catch(console.log);

