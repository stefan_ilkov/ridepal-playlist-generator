import db from './pool.js';

const getRandom = async (id) => {
  const sql = ` 
    SELECT t.id as track_id, t.title, t.duration, t.link, t.rank, t.preview_url, t.genre_id, 
    t.album_id, t.artist_id, g.name as genre, g.d_pic as genre_pic, ar.name as artist, 
    ar.d_pic as artist_pic, al.title as album, al.d_cover as album_cover, 
    al.d_release_date as album_release_date, al.d_fans as album_fans
    FROM tracks AS t
    JOIN genres g ON t.genre_id = g.d_id
    JOIN artists ar ON t.artist_id = ar.id
    JOIN albums al ON t.album_id = al.id 
    WHERE g.d_id = ?
    ORDER BY RAND()
    LIMIT 500;
  `;

  const result = await db.query(sql, [id]);
  return result.splice(0, result.length);
};

const getById = async (id) => {
  const sql = `
    SELECT pl.id, pl.title, pl.duration, pl.rank, pl.start_point, pl.end_point, pl.user_id, pl.is_deleted, pl.playlist_picture as pic,
    u.first_name, u.last_name, u.username, u.is_deleted as user_is_deleted
    FROM playlists AS pl
    JOIN users AS u ON pl.user_id=u.id
    WHERE pl.id = ?
    `;

  const result = await db.query(sql, [id]);
  const playlist = result[0];
  const genres = await getAllGenresByPlaylist(playlist.id);

  return { ...playlist, genres };
};

const getPlaylistByTitle = async (title, user_id) => {
  const sql = `SELECT * FROM playlists WHERE title = '${title}' AND user_id = ${user_id}`;

  const result = await db.query(sql);

  return result[0];
};

const createPlaylist = async (trackList, title, playlistDuration, rank, start_point, end_point, genres_id, user_id, image) => {
  const sql = `
  INSERT INTO playlists (title, duration, rank, start_point, end_point, user_id, playlist_picture)
  VALUES (?, ?, ?, ?, ?, ?, ?)
  `;

  const _ = await db.query(sql, [title, playlistDuration, rank, start_point, end_point, user_id, image]);

  const playlist = await getPlaylistByTitle(title, user_id);

  genres_id.map(async genre => {
    await db.query('INSERT INTO playlist_genres (playlist_id, genre_id) VALUES (?, ?)', [playlist.id, genre]);
  });

  trackList.map((async track => {
    await db.query('INSERT INTO track_lists (playlist_id, track_id) VALUES (?, ?)', [playlist.id, track.track_id]);
  }));

  return playlist;
};

const getAllPlaylists = async () => {
  const sql = `
    SELECT p.id, p.title, p.duration, p.rank, p.start_point, p.end_point, p.user_id, p.is_deleted, r.reaction, p.playlist_picture as pic,
    (SELECT COUNT(tracks.title) FROM tracks JOIN track_lists tl ON tracks.id=tl.track_id WHERE p.id = tl.playlist_id) as tracks,
    u.username
    FROM playlists AS p
    JOIN users u ON p.user_id=u.id
    LEFT JOIN playlist_reactions AS pl_r ON p.id = pl_r.playlist_id
    LEFT JOIN reactions AS r ON pl_r.reaction_id = r.id
    WHERE p.is_deleted = 0
    ORDER BY p.rank DESC
  `;

  const playlists = await db.query(sql);
  const playlistWithGenres = await Promise.all(playlists.map(async playlist => {
    const genres = await getAllGenresByPlaylist(playlist.id);
    return { ...playlist, genres };
  }));

  return playlistWithGenres;
};

const getAllPlaylistsAdmin = async () => {
  const sql = `
    SELECT p.id, p.title, p.duration, p.rank, p.start_point, p.end_point, p.user_id, p.is_deleted, r.reaction, p.playlist_picture as pic,
    (SELECT COUNT(tracks.title) FROM tracks JOIN track_lists tl ON tracks.id=tl.track_id WHERE p.id = tl.playlist_id) as tracks,
    u.username
    FROM playlists AS p
    JOIN users u ON p.user_id=u.id
    LEFT JOIN playlist_reactions AS pl_r ON p.id = pl_r.playlist_id
    LEFT JOIN reactions AS r ON pl_r.reaction_id = r.id
    ORDER BY p.rank DESC
  `;

  const playlists = await db.query(sql);
  const playlistWithGenres = await Promise.all(playlists.map(async playlist => {
    const genres = await getAllGenresByPlaylist(playlist.id);
    return { ...playlist, genres };
  }));

  return playlistWithGenres;
};


const getTopThree = async () => {
  const sql = `
    SELECT p.id, p.title, p.duration, p.rank, p.start_point, p.end_point, p.user_id, p.is_deleted, r.reaction, p.playlist_picture as pic,
    (SELECT COUNT(tracks.title) FROM tracks JOIN track_lists ON tracks.id=track_lists.track_id WHERE tracks.id = track_lists.track_id) as tracks
    FROM playlists AS p
    LEFT JOIN playlist_reactions AS pl_r ON p.id = pl_r.playlist_id
    LEFT JOIN reactions AS r ON pl_r.reaction_id = r.id
    WHERE p.is_deleted = 0
    ORDER BY p.rank DESC limit 3
  `;

  return await db.query(sql);
};


const getAllGenresByPlaylist = async (id) => {
  const sql = `
    SELECT g.name as genre
    FROM genres g
    JOIN playlist_genres pg ON pg.genre_id = g.d_id
    JOIN playlists p ON pg.playlist_id = p.id
    WHERE pg.playlist_id = ?
  `;

  const genres = [...await db.query(sql, [id])].slice();

  const str = genres.reduce((acc, val) => {
    return acc + val.genre + ', ';
  }, '');

  return str.slice(0, str.length - 2);
};

// const getAllGenresByPlaylist = async (id) => {
//   const sql = `
//     SELECT g.name as genre
//     FROM genres g
//     JOIN playlist_genres pg ON pg.genre_id = g.d_id
//     JOIN playlists p ON pg.playlist_id = p.id
//     WHERE pg.playlist_id = ?
//   `;

//   return await db.query(sql, [id]);
// };

const getAllTracksByPlaylist = async (id) => {
  const sql = `
    SELECT t.title, t.duration, t.rank, t.preview_url, g.name as genre, ar.d_pic as pic,
    ar.name as artist, al.title as album, al.d_release_date as release_date, al.d_fans as album_fans
    FROM tracks AS t
    JOIN track_lists tl ON t.id = tl.track_id
    JOIN genres g ON t.genre_id = g.d_id
    JOIN albums al ON t.album_id = al.id
    JOIN artists ar ON t.artist_id = ar.id
    WHERE tl.playlist_id = ?
  `;

  return await db.query(sql, [id]);
};

const update = async (id, title) => {
  const sql = `
    UPDATE playlists
    SET title = ?
    WHERE id = ?
  `;

  await db.query(sql, [title, id]);

  const sql2 = `
    SELECT * FROM playlists
    WHERE id = ?
  `;

  const result = await db.query(sql2, [id]);
  return result[0];
};

const remove = async (id) => {
  const sql = `
    UPDATE playlists
    SET is_deleted = 1
    WHERE id = ?
  `;

  await db.query(sql, [id]);

  const sql2 = `
    SELECT * FROM playlists
    WHERE id = ?
  `;

  const result = await db.query(sql2, [id]);
  return result[0];
};

const restore = async (id) => {
  const sql = `
    UPDATE playlists
    SET is_deleted = 0
    WHERE id = ?
  `;

  await db.query(sql, [id]);

  const sql2 = `
    SELECT * FROM playlists
    WHERE id = ?
  `;

  const result = await db.query(sql2, [id]);
  return result[0];
};


export default {
  getRandom,
  getById,
  getPlaylistByTitle,
  createPlaylist,
  getAllPlaylists,
  getAllPlaylistsAdmin,
  getTopThree,
  getAllGenresByPlaylist,
  getAllTracksByPlaylist,
  update,
  remove,
  restore,
};
