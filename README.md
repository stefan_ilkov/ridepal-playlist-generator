<img src="https://webassets.telerikacademy.com/images/default-source/logos/telerik-academy.svg" alt="logo" width="300px" style="margin-top: 20px;"/>

# RidePal Playlist Generator API

### 1. Project information

- Language and version: **JavaScript ES2020**
- Platform and version: **Node 14.0+**
- Core Packages: **Express**, **ESLint**, **React**
- Development team: **Iva & Stefan**

<br>

### 2. Description

`RidePal Playlist Generator` is an Application, which enables your users to generate playlists for specific travel duration periods, based on their preferred genres.

The files of the app are split into two parts - backend API application and front-end client application. First we will intoduce you to the backend part.

<br>

### 3. Server npm packages setup

In order to run the application you will need to set it up. You will be working in the `server` folder. This will be designated as the **root** folder, where `package.json` is placed.

You need to install all the packages in the root folder: Run `npm i` in the terminal.

The project can be run in two ways:

- `npm start` - will run the current version of the code and will **not** reflect any changes done to the code until this command is executed again
- `npm run start:dev` - will run the code in *development* mode, meaning every change done to the code will trigger the program to restart and reflect the changes made

<br>

### 4. Server-side project structure

The main part of the server-side code is in `server/src`:

- `src/index.js` - the entry point of the project
- `src/config.js` - configurations for the app deployments
- `src/auth` - contains all authentication setup and middleware
- `src/common` - contains all constrants and enums
- `src/controllers` - where all controller logic is
- `src/data` - contains the connection settings and functions for CRUD over todos in the database
- `src/middlewares` - contains all custom middleware
- `src/services` - where all services are
- `src/validators` - contains objects for validating body/query objects

<br>

### 5. Setup MySQL Workbench

We will be using **MySQL Workbench** for storing the data. If you don't have it installed, please do so. You can use the following link **[MySQL Workbench](https://www.mysql.com/products/workbench/)**.

We have provided you with the database which you can import in your MySQL Workbench - please use `playlistdb.sql` for the import. 

After you connect to the database `playlistdb` and examine the major tables, you can use the database for all tasks.

Your tables relations should look something like this:

![alt text](ScreenshotsProject/tables-database.png)

<br>

### 6. Run seed file

This will fetch data for 4 genres from the Deezer API and store them locally.

To fill the database with data you should type `npm run:seed` in the terminal. 

It will then fetch all artists for each genre from the Deezer Database. Albums are then fetched based on artists and the tracks are fetched based on the albums. Desired enres can be specified in the database by altering the if statement, however we have chosen these genres due to having high number of artists, albums and tracks, which is why we recommend running the application with them. Roles for admin and for user are inserted. Reactions are also inserted and will be utilised in future versions of the app. Finally, 2 users are inserted with admin roles. They can be customized and additional users can also be added if desired. Specification is needed for the user role, username, password, first name, last name, and email.

<br>

### 7. Dotenv

You will be using dotenv to help manage your environment variables. The **.env** file is stored in your .gitignore file.
You will have to create it in the **root** folder, where `package.json` is placed, as a separate file. 

You can use the following content for your **.env** file. Keep in mind that you might need to replace `USER` and `PASSWORD` with the ones you have set in your `MySQL Workbench`. Also make sure that `DATABASE` name corresponds to the one with the data you will be using to run the application.

```
PORT=5555
HOST=localhost
DBPORT=3306
USER=root
PASSWORD=root
DATABASE=playlistdb
PRIVATE_KEY=secret_key
```

<br>

### 8. Working with Postman

**Postman** is a great tool for testing the API you will be running. In order to test the api you can use the **[Postman](https://www.getpostman.com/downloads/)** tool. You can **test the API** for the different tasks and cases.

<br>

### 9. Client npm packages setup

You will need to set it up the front-end part of the application. You will be working in the `client` folder. This will be designated as the **root** folder, where `package.json` is placed.

You need to install all the packages in the root folder: `npm i`.

To run the client enter `npm start` in the terminal.

<br>

### 10. Front-End views and functionalities

**Home page views:**


Home screen:

![alt text](ScreenshotsProject/home.png)
![alt text](ScreenshotsProject/homeTopPlaylists.png)

Home screen - user:

![alt text](ScreenshotsProject/homeUser.png)

Home screen - admin:

![alt text](ScreenshotsProject/homeAdmin.png)

**Profiles and account functionalities:**

Profile - user:

![alt text](ScreenshotsProject/userAccount.png)

Profile - admin:

![alt text](ScreenshotsProject/adminPlofile.png)

Admin Panel - playlists
![alt text](ScreenshotsProject/adminPanel1.png)

Admin Panel- edit playlist
![alt text](ScreenshotsProject/adminPanel2.png)

Admin panel - delete playlist
![alt text](ScreenshotsProject/adminPanel3.png)

Admin Panel - users
![alt text](ScreenshotsProject/adminPanel4.png)

Admin Panel - edit user
![alt text](ScreenshotsProject/adminPanel5.png)

Admin panel - delete user
![alt text](ScreenshotsProject/adminPanel6.png)

**Playlists:**

Display all playlists

![alt text](ScreenshotsProject/displayPlaylists.png)

Display single playlist

![alt text](ScreenshotsProject/viewPlaylist.png)

Display single playlist - tracks

![alt text](ScreenshotsProject/viewPlaylist-tracks.png)

Edit playlist

![alt text](ScreenshotsProject/editPlaylist.png)

Delete playlist

![alt text](ScreenshotsProject/deletePlaylist.png)

**Authentication**

Login

![alt text](ScreenshotsProject/login.png)

Signup

![alt text](ScreenshotsProject/signup1.png)

![alt text](ScreenshotsProject/signup2.png)

**Generate palylist**

![alt text](ScreenshotsProject/generateStep1.png)
![alt text](ScreenshotsProject/generateStep2.png)
![alt text](ScreenshotsProject/generateStep2-2.png)
![alt text](ScreenshotsProject/generateStep3-1.png)
![alt text](ScreenshotsProject/generateStep3-2.png)
![alt text](ScreenshotsProject/generateFinal-1.png)
![alt text](ScreenshotsProject/generateFinal-2.png)

